<?php 

$categorySlug = get_the_category()[0]->slug;

$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;

// global $query_string;
// $paged = (get_query_var('page')) ? get_query_var('page') : 1;
// query_posts($query_string."paged=$paged&posts_per_page=1");
// $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

$query_args = array(
  'post_type' => 'post',
  'category_name' => $category->cat_name,
  'posts_per_page' => $get_option['posts_per_page'],
  'paged' => $paged
);

// the query
$the_query = new WP_Query( $query_args ); ?>

<?php if ( $the_query->have_posts() ) : ?>

  <!-- pagination here -->

<!-- the loop -->
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


<!-- #content -->
<div id="content" style="width: 100%;">
	<!-- .post-content -->
	<div class="post-content yen-news post-tips-striped">

		
					
		<!-- Full width #1 -->
		<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-1  fusion-parallax-none nonhundred-percent-fullwidth cat_archive" style="border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-top:20px;padding-left:;padding-right:;background-color:rgba(255,255,255,0);">
		
            <div class="fusion-row">
            			<!-- first -->
            			<div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

						<?php echo get_the_post_thumbnail( null, 'post-thumbnail', '' ) ?>

<div class="fusion-clearfix"></div></div></div>
						<!-- last-column -->
						<div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;">
							<div class="fusion-column-wrapper">

<?php echo get_the_category_list(); ?>
								
								<h2 class="yen-news-title">
									<a href="<?php echo get_the_permalink() ?>"><?php echo get_the_title() ?></a></h2>
									<p><?php echo get_the_excerpt(); ?></p>
								<p class="p1" style="margin-top: 40px"><span class="s1">by <?php echo get_the_author() ?>, <?php echo get_the_date() ?></span></p>
								<div class="fusion-clearfix"></div>
							</div>
						</div>
						<!-- Clearfix -->
						<div class="fusion-clearfix"></div>
			</div>
		</div>

		

		

	</div>
</div>

	 <?php endwhile; ?>
  	 <!-- end of the loop -->

	<!-- pagination here -->
	<?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
		<div class="fusion-clearfix"></div>
		<div class="yen-paginate-wp">
			<?php echo paginate_links() ?>
		</div>
	<?php } ?>

	<?php wp_reset_postdata(); ?>

<?php else:  ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>