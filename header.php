<!DOCTYPE html>
<?php global $woocommerce; ?>
<html class="<?php echo ( Avada()->settings->get( 'smooth_scrolling' ) ) ? 'no-overflow-y' : ''; ?>" <?php language_attributes(); ?>>
<head>
	<?php if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ( false !== strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE' ) ) ) : ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<?php endif; ?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<?php
	if ( ! function_exists( '_wp_render_title_tag' ) ) {
		function avada_render_title() {
		?>
			<title><?php wp_title( '' ); ?></title>
		<?php
		}
		add_action( 'wp_head', 'avada_render_title' );
	}
	?>

	<!--[if lte IE 8]>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/html5shiv.js"></script>
	<![endif]-->

	<?php $isiPad = (bool) strpos( $_SERVER['HTTP_USER_AGENT'],'iPad' ); ?>

	<?php
	$viewport = '';
	if ( Avada()->settings->get( 'responsive' ) && $isiPad ) {
		$viewport = '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />';
	} else if ( Avada()->settings->get( 'responsive' ) ) {
		if ( Avada()->settings->get( 'mobile_zoom' ) ) {
			$viewport .= '<meta name="viewport" content="width=device-width, initial-scale=1" />';
		} else {
			$viewport .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />';
		}
	}

	$viewport = apply_filters( 'avada_viewport_meta', $viewport );
	echo $viewport;
	?>

	<?php wp_head(); ?>

	<?php

	$object_id = get_queried_object_id();
	$c_pageID  = Avada::c_pageID();
	?>

	<!--[if lte IE 8]>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	var imgs, i, w;
	var imgs = document.getElementsByTagName( 'img' );
	for( i = 0; i < imgs.length; i++ ) {
		w = imgs[i].getAttribute( 'width' );
		imgs[i].removeAttribute( 'width' );
		imgs[i].removeAttribute( 'height' );
	}
	});
	</script>

	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/excanvas.js"></script>

	<![endif]-->

	<!--[if lte IE 9]>
	<script type="text/javascript">
	jQuery(document).ready(function() {

	// Combine inline styles for body tag
	jQuery('body').each( function() {
		var combined_styles = '<style type="text/css">';

		jQuery( this ).find( 'style' ).each( function() {
			combined_styles += jQuery(this).html();
			jQuery(this).remove();
		});

		combined_styles += '</style>';

		jQuery( this ).prepend( combined_styles );
	});
	});
	</script>

	<![endif]-->

	<script type="text/javascript">
		var doc = document.documentElement;
		doc.setAttribute('data-useragent', navigator.userAgent);
	</script>

	<?php echo Avada()->settings->get( 'google_analytics' ); ?>

	<?php echo Avada()->settings->get( 'space_head' ); ?>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<?php if (is_page(2) || is_page(385)): ?>
	<style>

/* fix navbar tab homepage */
@media (max-width: 768px) {
  .fusion-tabs .nav {
    display: block;
  }
  .fusion-tabs .fusion-mobile-tab-nav {
    display: none;
  }
  .fusion-tabs.clean .tab-pane {
    margin: 0;
  }
  .fusion-tabs .nav-tabs {
    display: inline-block;
    vertical-align: middle;
  }
  .fusion-tabs .nav-tabs.nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .fusion-tabs .nav-tabs li .tab-link {
    margin-right: 1px;
  }
  .fusion-tabs .nav-tabs li:last-child .tab-link {
    margin-right: 0;
  }
  .fusion-tabs.horizontal-tabs .nav-tabs {
    margin: 0 0 -1px 0;
  }
  .fusion-tabs.horizontal-tabs .nav {
    border-bottom: 1px solid #f1f2f2;
  }
  .fusion-tabs.horizontal-tabs.clean .nav {
    border: none;
    text-align: center;
  }
  .fusion-tabs.horizontal-tabs.clean .nav-tabs {
    border: none;
  }
  .fusion-tabs.horizontal-tabs.clean .nav-tabs li {
    margin-bottom: 0;
  }
  .fusion-tabs.horizontal-tabs.clean .nav-tabs li .tab-link {
    margin-right: -1px;
  }
  .fusion-tabs.horizontal-tabs.clean .tab-content {
    margin-top: 0px;
  }
  .fusion-tabs.nav-not-justified {
    border: none;
  }
  .fusion-tabs.nav-not-justified .nav-tabs li {
    display: inline-block;
  }
  .fusion-tabs.nav-not-justified.clean .nav-tabs li .tab-link {
    padding: 14px 55px;
  }
  .fusion-tabs.vertical-tabs {
    border: none;
    clear: both;
    zoom: 1;
  }
  .fusion-tabs.vertical-tabs:before,
  .fusion-tabs.vertical-tabs:after {
    content: " ";
    display: table;
  }
  .fusion-tabs.vertical-tabs:after {
    clear: both;
  }
  .fusion-tabs.vertical-tabs .nav-tabs {
    position: relative;
    left: 1px;
    float: left;
    width: 15.5%;
    border: 1px solid #f1f2f2;
    border-right: none;
  }
  .fusion-tabs.vertical-tabs .nav-tabs > li .tab-link {
    margin-right: 0;
    margin-bottom: 1px;
    padding: 8px 10px;
    border-top: none;
    text-align: left;
  }
  .fusion-tabs.vertical-tabs .nav-tabs > li:last-child .tab-link {
    margin-bottom: 0;
  }
  .fusion-tabs.vertical-tabs .nav-tabs > li.active > .tab-link {
    border-bottom: none;
    border-left: 3px solid #a0ce4e;
    border-top: none;
    cursor: pointer;
  }
  .fusion-tabs.vertical-tabs .tab-content {
    float: left;
    width: 84.5%;
  }
  .fusion-tabs.vertical-tabs .tab-pane {
    float: left;
    padding: 30px;
    border: 1px solid #f1f2f2;
  }
  .fusion-tabs.vertical-tabs.clean .nav-tabs {
    width: 25%;
    background-color: transparent;
    border: none;
  }
  .fusion-tabs.vertical-tabs.clean .nav-tabs li .tab-link {
    margin: 0;
    padding: 10px 15px;
    border: 1px solid;
  }
  .fusion-tabs.vertical-tabs.clean .tab-content {
    margin: 0;
    padding-left: 40px;
    width: 75%;
  }
}
/* fix navbar tab homepage */

@media  (min-width: 992px) {
  .statsWrapper {    
    display: -webkit-flex;    
    display: -webkit-flex;    
    display: -webkit-flex;
    display: flex;    
    text-align: center;
  }
  .statsWrapper h1 {    
    margin-top: 40px;    
    margin-bottom: 24px;    
    padding: 0;    
    font-size: 67px;    
    font-weight: bold;
  }
  .statsWrapper small {    
    display: block;    
    margin-bottom: 21px;    
    font-size: 16px;    
    color: #9c9c9c;
  }
  .formationStatsWrapper {    
    -webkit-order: 1;    
    -webkit-order: 1;    -webkit-order: 1;
    order: 1;    
    width: 33%;    
    margin-right: 3%;    
    border: 1px solid #eee;
  }
  .formationStatsWrapper h1 {    
    color: orange;
  }
  .formationStatsWrapper .widgetHeader {    
    text-transform: uppercase;    
    background: orange;    
    color: white;
  }
  .validationStatsWrapper {    
    -webkit-order: 2;    
    -webkit-order: 2;    -webkit-order: 2;
    order: 2;    
    width: 33%;    
    margin-right: 3%;    
    border: 1px solid #eee;
  }
  .validationStatsWrapper h1 {    
    color: #75c1df;
  }
  .validationStatsWrapper .widgetHeader {    
    text-transform: uppercase;    
    background: #75c1df;    
    color: white;
  }
  .growthStatsWrapper {    
    -webkit-order: 3;    
    -webkit-order: 3;    -webkit-order: 3;
    order: 3;    
    width: 33%;    
    border: 1px solid #eee;
  }
  .growthStatsWrapper h1 {    
    color: #213143;
  }
  .growthStatsWrapper .widgetHeader {    
    text-transform: uppercase;    
    background: #213143;    
    color: white;
  }
}


@media  (min-width: 1400px) {
  .statsWrapper h1 {    
    margin-top: 9px;    
    margin-bottom: 14px;    
    padding: 0;    
    font-size: 67px;    
    font-weight: bold;
  }
}

@media(max-width:577px) {
  .statsWrapper {
	flex-direction: column !important;
	}
	.growthStatsWrapper,
	.validationStatsWrapper,
	.formationStatsWrapper
	 {
		width: 100% !important;
		margin-right: 0% !important;
margin-bottom: 3%;
	}
}


@media  (max-width: 991px) {
  .statsWrapper {    
    display: -webkit-flex;    
    display: -webkit-flex;    
    display: -webkit-flex;
    display: flex;    
    text-align: center;
  }
  .statsWrapper h1 {    
    margin-top: 40px;    
    margin-bottom: 24px;    
    padding: 0;    
    font-size: 67px;    
    font-weight: bold;
  }
  .statsWrapper small {    
    display: block;    
    margin-bottom: 21px;    
    font-size: 16px;    
    color: #9c9c9c;
  }
  .formationStatsWrapper {    
    -webkit-order: 1;    
    -webkit-order: 1;    -webkit-order: 1;
    order: 1;    
    width: 33%;    
    margin-right: 3%;    
    border: 1px solid #eee;
  }
  .formationStatsWrapper h1 {    
    color: orange;
  }
  .formationStatsWrapper .widgetHeader {    
    text-transform: uppercase;    
    background: orange;    
    color: white;
  }
  .validationStatsWrapper {    
    -webkit-order: 2;    
    -webkit-order: 2;    -webkit-order: 2;
    order: 2;    
    width: 33%;    
    margin-right: 3%;    
    border: 1px solid #eee;
  }
  .validationStatsWrapper h1 {    
    color: #75c1df;
  }
  .validationStatsWrapper .widgetHeader {    
    text-transform: uppercase;    
    background: #75c1df;    
    color: white;
  }
  .growthStatsWrapper {    
    -webkit-order: 3;    
    -webkit-order: 3;    -webkit-order: 3;
    order: 3;    
    width: 33%;    
    border: 1px solid #eee;
  }
  .growthStatsWrapper h1 {    
    color: #213143;
  }
  .growthStatsWrapper .widgetHeader {    
    text-transform: uppercase;    
    background: #213143;    
    color: white;
  }
}


.finLabelWrapper {
    text-align: center !important;
    padding: 20px !important;
}
.finLabel {
    padding-right: 10px !important;
    padding-left: 10px !important;
    padding-top: 5px;
    padding-bottom: 5px !important;
    border-radius: 20px !important;
    color: #fff !important;
}

#finWidget div:first-child .finLabel {
/*    background: #75c1df !important; */
}

#finWidget div:nth-child(2) .finLabel {
/*    background: #e0940d !important; */
}

#finWidget div:nth-child(3) .finLabel {
/*    background: #213143 !important; */
}

.yen-tabs {
margin-bottom:0px;
}
		@media  (min-width: 768px) {
  .container-home-latest, .container-home-random {    
    display: -webkit-flex;
    display: flex;
  }
  .column {    -ms-flex: 1;
    -webkit-flex: 1;
    flex: 1;    /*for demo purposes only */    
    background: #f2f2f2;    
    box-sizing: border-box;
  }
  .column-1 {    -webkit-order: 1;
    order: 1;
  }
  .column-2 {    -webkit-order: 2;
    order: 2;
  }
  .column-3 {    -webkit-order: 3;
    order: 3;
  }
}

.container-home-latest .wrapper {
    position: relative;
}

.container-home-latest .wrapper .featured-img {
    min-height: 250px;
    width: 100%;
    background-size: cover;
    background-position: center center;
}

.container-home-latest .wrapper .featured-title {
    position: absolute;
    bottom: 0;
    width: 100%;
    padding: 10px;
    color: white;
    font-size: 14px;
}

.column-1 .featured-title {
    background-color: rgba(253, 162, 62, .9);
}

.column-2 .featured-title {
    background-color: rgba(41, 57, 74, .9);
}

.column-3 .featured-title {
    background-color: rgba(240, 88, 90, .9);
}

/*RANDOM*/

.container-home-random .wrapper {
    position: relative;
}

.container-home-random .wrapper .featured-img {
    min-height: 350px;
    width: 100%;
    background-size: cover;
    background-position: center center;
}

.container-home-random .wrapper .featured-random-title {
    position: absolute;
    bottom: 0;
    width: 100%;
    padding: 10px;
    color: white;
    font-size: 14px;
}

.column-1 .featured-random-title {
    background-color: rgba(253, 162, 62, .9);
}

.column-2 .featured-random-title {
    background-color: rgba(240, 88, 90, .9);
}

@media  (max-width: 768px) {
  .container-home-latest, .container-home-random {    
    display: block;
  }
}
	</style>
<?php endif ?>

<?php if (is_page(11)): ?>
<style>
@media(max-width: 768px) {
	.fusion-column-wrapper {
		padding: 0px !important;
	}
	.fix-mobile-fullwidth {
		padding: 0px !important;
	}
}
</style>
<?php endif ?>

<style>
@media(max-width: 768px) {
	.fix-font-size-mobile {
		font-size: 24px !important;
	}
	ul.post-categories li {
		margin-bottom: 6px !important;
	}
	.fusion-page-title-bar .entry-title {
		padding: 40px 0px;
	}
}



</style>

</head>
<?php
$wrapper_class = '';


if ( is_page_template( 'blank.php' ) ) {
	$wrapper_class  = 'wrapper_blank';
}

if ( 'modern' == Avada()->settings->get( 'mobile_menu_design' ) ) {
	$mobile_logo_pos = strtolower( Avada()->settings->get( 'logo_alignment' ) );
	if ( 'center' == strtolower( Avada()->settings->get( 'logo_alignment' ) ) ) {
		$mobile_logo_pos = 'left';
	}
}

?>
<body <?php body_class(); ?>>
	<?php do_action( 'avada_before_body_content' ); ?>
	<?php $boxed_side_header_right = false; ?>
	<?php if ( ( ( 'Boxed' == Avada()->settings->get( 'layout' ) && ( 'default' == get_post_meta( $c_pageID, 'pyre_page_bg_layout', true ) || '' == get_post_meta( $c_pageID, 'pyre_page_bg_layout', true ) ) ) || 'boxed' == get_post_meta( $c_pageID, 'pyre_page_bg_layout', true ) ) && 'Top' != Avada()->settings->get( 'header_position' ) ) : ?>
		<?php if ( Avada()->settings->get( 'slidingbar_widgets' ) && ! is_page_template( 'blank.php' ) && ( 'Right' == Avada()->settings->get( 'header_position' ) || 'Left' == Avada()->settings->get( 'header_position' ) ) ) : ?>
			<?php get_template_part( 'slidingbar' ); ?>
			<?php $boxed_side_header_right = true; ?>
		<?php endif; ?>
		<div id="boxed-wrapper">
	<?php endif; ?>
	<div id="wrapper" class="<?php echo $wrapper_class; ?>">
		<div id="home" style="position:relative;top:1px;"></div>
		<?php if ( Avada()->settings->get( 'slidingbar_widgets' ) && ! is_page_template( 'blank.php' ) && ! $boxed_side_header_right ) : ?>
			<?php get_template_part( 'slidingbar' ); ?>
		<?php endif; ?>
		<?php if ( false !== strpos( Avada()->settings->get( 'footer_special_effects' ), 'footer_sticky' ) ) : ?>
			<div class="above-footer-wrapper">
		<?php endif; ?>

		<?php avada_header_template( 'Below' ); ?>
		<?php if ( 'Left' == Avada()->settings->get( 'header_position' ) || 'Right' == Avada()->settings->get( 'header_position' ) ) : ?>
			<?php avada_side_header(); ?>
		<?php endif; ?>

		<div id="sliders-container">
			<?php
			if ( is_search() ) {
				$slider_page_id = '';
			} else {
				$slider_page_id = '';
				if ( ! is_home() && ! is_front_page() && ! is_archive() && isset( $object_id ) ) {
					$slider_page_id = $object_id;
				}
				if ( ! is_home() && is_front_page() && isset( $object_id ) ) {
					$slider_page_id = $object_id;
				}
				if ( is_home() && ! is_front_page() ) {
					$slider_page_id = get_option( 'page_for_posts' );
				}
				if ( class_exists( 'WooCommerce' ) && is_shop() ) {
					$slider_page_id = get_option( 'woocommerce_shop_page_id' );
				}

				if ( ( 'publish' == get_post_status( $slider_page_id ) && ! post_password_required() ) || ( current_user_can( 'read_private_pages' ) && in_array( get_post_status( $slider_page_id ), array( 'private', 'draft', 'pending' ) ) ) ) {
					avada_slider( $slider_page_id );
				}
			} ?>
		</div>
		<?php if ( get_post_meta( $slider_page_id, 'pyre_fallback', true ) ) : ?>
			<div id="fallback-slide">
				<img src="<?php echo get_post_meta( $slider_page_id, 'pyre_fallback', true ); ?>" alt="" />
			</div>
		<?php endif; ?>
		<?php avada_header_template( 'Above' ); ?>

		<?php if ( has_action( 'avada_override_current_page_title_bar' ) ) : ?>
			<?php do_action( 'avada_override_current_page_title_bar', $c_pageID ); ?>
		<?php else : ?>
			<?php avada_current_page_title_bar( $c_pageID ); ?>
		<?php endif; ?>

		<?php if ( is_page_template( 'contact.php' ) && Avada()->settings->get( 'recaptcha_public' ) && Avada()->settings->get( 'recaptcha_private' ) ) : ?>
			<script type="text/javascript">var RecaptchaOptions = { theme : '<?php echo Avada()->settings->get( 'recaptcha_color_scheme' ); ?>' };</script>
		<?php endif; ?>

		<?php if ( is_page_template( 'contact.php' ) && Avada()->settings->get( 'gmap_address' ) && Avada()->settings->get( 'status_gmap' ) ) : ?>
			<?php
			$map_popup             = ( ! Avada()->settings->get( 'map_popup' ) )          ? 'yes' : 'no';
			$map_scrollwheel       = ( Avada()->settings->get( 'map_scrollwheel' ) )    ? 'yes' : 'no';
			$map_scale             = ( Avada()->settings->get( 'map_scale' ) )          ? 'yes' : 'no';
			$map_zoomcontrol       = ( Avada()->settings->get( 'map_zoomcontrol' ) )    ? 'yes' : 'no';
			$address_pin           = ( Avada()->settings->get( 'map_pin' ) )            ? 'yes' : 'no';
			$address_pin_animation = ( Avada()->settings->get( 'gmap_pin_animation' ) ) ? 'yes' : 'no';
			?>
			<div id="fusion-gmap-container">
				<?php echo Avada()->google_map->render_map( array(
					'address'                  => Avada()->settings->get( 'gmap_address' ),
					'type'                     => Avada()->settings->get( 'gmap_type' ),
					'address_pin'              => $address_pin,
					'animation'                => $address_pin_animation,
					'map_style'                => Avada()->settings->get( 'map_styling' ),
					'overlay_color'            => Avada()->settings->get( 'map_overlay_color' ),
					'infobox'                  => Avada()->settings->get( 'map_infobox_styling' ),
					'infobox_background_color' => Avada()->settings->get( 'map_infobox_bg_color' ),
					'infobox_text_color'       => Avada()->settings->get( 'map_infobox_text_color' ),
					'infobox_content'          => htmlentities( Avada()->settings->get( 'map_infobox_content' ) ),
					'icon'                     => Avada()->settings->get( 'map_custom_marker_icon' ),
					'width'                    => Avada()->settings->get( 'gmap_dimensions', 'width' ),
					'height'                   => Avada()->settings->get( 'gmap_dimensions', 'height' ),
					'zoom'                     => Avada()->settings->get( 'map_zoom_level' ),
					'scrollwheel'              => $map_scrollwheel,
					'scale'                    => $map_scale,
					'zoom_pancontrol'          => $map_zoomcontrol,
					'popup'                    => $map_popup
				) ); ?>
			</div>
		<?php endif; ?>

		<?php if ( is_page_template( 'contact-2.php' ) && Avada()->settings->get( 'gmap_address' ) && Avada()->settings->get( 'status_gmap' ) ) : ?>
			<?php
			$map_popup             = ( ! Avada()->settings->get( 'map_popup' ) )          ? 'yes' : 'no';
			$map_scrollwheel       = ( Avada()->settings->get( 'map_scrollwheel' ) )    ? 'yes' : 'no';
			$map_scale             = ( Avada()->settings->get( 'map_scale' ) )          ? 'yes' : 'no';
			$map_zoomcontrol       = ( Avada()->settings->get( 'map_zoomcontrol' ) )    ? 'yes' : 'no';
			$address_pin_animation = ( Avada()->settings->get( 'gmap_pin_animation' ) ) ? 'yes' : 'no';
			?>
			<div id="fusion-gmap-container">
				<?php echo Avada()->google_map->render_map( array(
					'address'                  => Avada()->settings->get( 'gmap_address' ),
					'type'                     => Avada()->settings->get( 'gmap_type' ),
					'map_style'                => Avada()->settings->get( 'map_styling' ),
					'animation'                => $address_pin_animation,
					'overlay_color'            => Avada()->settings->get( 'map_overlay_color' ),
					'infobox'                  => Avada()->settings->get( 'map_infobox_styling' ),
					'infobox_background_color' => Avada()->settings->get( 'map_infobox_bg_color' ),
					'infobox_text_color'       => Avada()->settings->get( 'map_infobox_text_color' ),
					'infobox_content'          => htmlentities( Avada()->settings->get( 'map_infobox_content' ) ),
					'icon'                     => Avada()->settings->get( 'map_custom_marker_icon' ),
					'width'                    => Avada()->settings->get( 'gmap_dimensions', 'width' ),
					'height'                   => Avada()->settings->get( 'gmap_dimensions', 'height' ),
					'zoom'                     => Avada()->settings->get( 'map_zoom_level' ),
					'scrollwheel'              => $map_scrollwheel,
					'scale'                    => $map_scale,
					'zoom_pancontrol'          => $map_zoomcontrol,
					'popup'                    => $map_popup
				) ); ?>
			</div>
		<?php endif; ?>
		<?php
		$main_css      = '';
		$row_css       = '';
		$main_class    = '';

		if ( Avada()->layout->is_hundred_percent_template() ) {
			$main_css = 'padding-left:0px;padding-right:0px;';
			if ( Avada()->settings->get( 'hundredp_padding' ) && ! get_post_meta( $c_pageID, 'pyre_hundredp_padding', true ) ) {
				$main_css = 'padding-left:' . Avada()->settings->get( 'hundredp_padding' ) . ';padding-right:' . Avada()->settings->get( 'hundredp_padding' );
			}
			if ( get_post_meta( $c_pageID, 'pyre_hundredp_padding', true ) ) {
				$main_css = 'padding-left:' . get_post_meta( $c_pageID, 'pyre_hundredp_padding', true ) . ';padding-right:' . get_post_meta( $c_pageID, 'pyre_hundredp_padding', true );
			}
			$row_css    = 'max-width:100%;';
			$main_class = 'width-100';
		}
		do_action( 'avada_before_main_container' );
		?>
		<div id="main" class="clearfix <?php echo $main_class; ?>" style="<?php echo $main_css; ?>">
			<div class="fusion-row" style="<?php echo $row_css; ?>">